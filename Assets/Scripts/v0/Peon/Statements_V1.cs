﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statements_V1 : MonoBehaviour {
    public const float intervalTime = 5;
    //current time
    private float time;
    private RaycastHit hit;
    [SerializeField][Readonly] private int _currentState;

    public enum PeonActions {
        WAITING = 0,
        MOVING = 1
    }
    // Use this for initialization
    void Start () {
        gameObject.GetComponent<Renderer>().material.color = Color.red;
        time = 0;
        _currentState = (int)PeonActions.MOVING;
    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;        
        if (time >= intervalTime)
        {
            //if (Random.Range(0, 10) > 7)
            //{
                transform.Rotate(Vector3.up * Random.Range(50, 200));
                _currentState = (int)PeonActions.MOVING;
            //}
            //else
            //{
                //_currentState = (int)PeonActions.WAITING;
            //}
            time = 0;
        }

        if (_currentState == (int)PeonActions.MOVING)
            moving(); 
        else waiting();
    }

    public void waiting() {
        
        //Debug.Log("Waiting");
    }

    public void moving() {
        transform.Translate(Vector3.forward * 5 * Time.deltaTime);
        //if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 3)){
            /*bool blocked = NavMesh.Raycast(transform.position, target.position, out hit, NavMesh.AllAreas);
            Debug.DrawLine(transform.position, target.position, blocked ? Color.red : Color.green);
            if (blocked)
                Debug.DrawRay(hit.position, Vector3.up, Color.red);
            //Debug.Log("Hello");
            Debug.Log(hit.collider.name);*/
        //}
        //Debug.Log("Moving");
    }
}
