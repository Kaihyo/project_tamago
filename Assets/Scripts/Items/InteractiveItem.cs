﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class InteractiveItem : MonoBehaviour {

    public ItemType type;
    public StatsRates statsValue;
    public int eatReward, sleepReward, poopReward, playReward;
}
