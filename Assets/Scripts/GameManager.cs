﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public RectTransform hungerSlider, staminaSlider, hygieneSlider, entertainmentSlider;
    public CreatureBody creature;
    [Range(1,10)]
    public float gameSpeed = 1.0f;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get {
            if(_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }

            if(_instance == null)
            {
                GameObject obj = new GameObject("@GameManager");
                _instance = obj.AddComponent<GameManager>();
                Debug.Log("Could not locate a GameManager object. \n GameManager was generated automaticly");
            }

            return _instance;
        }
    }
	
	// Update is called once per frame
	void Update () {
        UpdateSliders();
    }

    void UpdateSliders()
    {
        hungerSlider.anchorMax = new Vector2(hungerSlider.anchorMax.x, creature.stats.Hunger);
        staminaSlider.anchorMax = new Vector2(staminaSlider.anchorMax.x, creature.stats.Stamina);
        hygieneSlider.anchorMax = new Vector2(hygieneSlider.anchorMax.x, creature.stats.Hygiene);
        entertainmentSlider.anchorMax = new Vector2(entertainmentSlider.anchorMax.x, creature.stats.Entertainment);
    }
}
