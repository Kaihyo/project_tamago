﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CreatureAction{
    public CreatureRawAction rawAction;
    public GameObject target;

    public bool CompareWith(CreatureAction other)
    {
        bool output = rawAction == other.rawAction ? true : false;
        output &= target == other.target ? true : false;

        return output;
    }
}