﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureFace : MonoBehaviour {

    public GameObject holder;

    public Sprite neutralFace;
    public List<Sprite> basicNeedFaces;
    public List<Sprite> urgentNeedFaces;
    public List<Sprite> incidentFaces;
    public List<Sprite> doingWellFaces;

    private SpriteRenderer _renderer;
    public Sprite _currentFace;

    void Start()
    {
        if (holder == null)
        {
            holder = Instantiate(new GameObject("Face"));
            holder.AddComponent<SpriteRenderer>();
            holder.transform.position = new Vector3(0, 2, 0);
            holder.transform.rotation = Quaternion.Euler(90, 0, 0);
            holder.transform.parent = transform;
        }

        if (neutralFace != null)
            _currentFace = neutralFace;
        else
        {
            Object[] faceSprites = Resources.LoadAll("Creature_Faces");
            _currentFace = (Sprite)faceSprites[7];
        }

        _renderer = holder.GetComponent<SpriteRenderer>();
        _renderer.sprite = _currentFace;
    }

    public void UpdateFace(CreatureNeed mainNeed, CreatureStats stats)
    {
        switch (mainNeed)
        {
            case CreatureNeed.HUNGRY:
                _currentFace = stats.Hunger <= 0.25 ? urgentNeedFaces[0] : basicNeedFaces[0]; 
                break;
            case CreatureNeed.TIRED:
                _currentFace = stats.Stamina <= 0.25 ? urgentNeedFaces[1] : basicNeedFaces[1];
                break;
            case CreatureNeed.DIRTY:
                _currentFace = stats.Hygiene <= 0.25 ? urgentNeedFaces[2] : basicNeedFaces[2];
                break;
            case CreatureNeed.BORED:
                _currentFace = stats.Entertainment <= 0.25 ? urgentNeedFaces[3] : basicNeedFaces[3];
                break;
            default:
                _currentFace = neutralFace;
                break;
        }

        _renderer.sprite = _currentFace;
    }

    public void UpdateFace(CreatureRawAction currentAction, bool forcedActing)
    {
        switch (currentAction)
        {
            case CreatureRawAction.EATING:
                _currentFace = doingWellFaces[0];
                break;
            case CreatureRawAction.SLEEPING:
                _currentFace = forcedActing ? incidentFaces[1] : doingWellFaces[1];
                break;
            case CreatureRawAction.POOPING:
                _currentFace = forcedActing ? incidentFaces[2] : doingWellFaces[2];
                break;
            case CreatureRawAction.PLAYING:
                _currentFace = doingWellFaces[3];
                break;
            default:
                _currentFace = neutralFace;
                break;
        }

        _renderer.sprite = _currentFace;
    }
}
