﻿public enum CreatureNeed
{
    HUNGRY,
    TIRED,
    DIRTY,
    BORED,
    NOTHING
}