﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureAI : MonoBehaviour {

    private const int _TEMPERATURE_CEILING = 10000;
    private const float _MIN_TEMPERATURE = 1.0f / 50.0f;
    private const float _MAX_TEMPERATURE = 1.0f / 2.0f;

    public List<CreatureState> states;
    public List<CreatureAction> actions;

    private CreatureBody _body;
    private float _alpha = 0.1f;
    private float _gamma = 0.9f;
    private float[,] _q;
    private int[,] _r;
    private bool _learning = false;
    private int _nbOfExperience;

    void Start()
    {
        //à supprimer quand les tests sont finis
        Application.runInBackground = true;

        _body = GetComponent<CreatureBody>();

        if(states == null)
        {
            states = new List<CreatureState>();
        }

        if(actions == null)
        {
            actions = new List<CreatureAction>();
        }

        _q = new float[states.Count, actions.Count];
        _r = new int[states.Count, actions.Count];

        // initialisation arbitraire des récompenses
        Init();
    }

    void Update()
    {
		if (Input.GetKeyDown (KeyCode.D)) {
			Sherlock ();
		}

        if (!_learning && _body.state.mainNeed != CreatureNeed.NOTHING)
        {
            StartCoroutine(QLearn());
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            PrintResult();
            ShowPolicy();
        }/**/
    }

	void Sherlock(){
		CreatureState state = new CreatureState ();
		state.mainNeed = CreatureNeed.NOTHING;
		state.target = GameObject.Find("Food");

		Debug.Log (StateToId(state));

		Debug.Log ("state: " + states [4].mainNeed + " target: " + states [4].target);
	}

    void OnApplicationQuit()
    {
        Debug.Log("Nb of experiences : " + _nbOfExperience);
        PrintResult();
        ShowPolicy();/**/
    }

    void Init()
    {
        //initialisation arbitraire des récompenses
        for(int i = 0; i < _r.GetLength(0); i++)
        {
            for(int j = 0; j < 4; j++)
            {
                _r[i, j] = -50;
            }
        }

        for (int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                _r[i * 5 + j, j] = 1;
            }
        }
    }

    int StateToId(CreatureState state)
    {
        int id = -1;

        for(int i = 0; i < states.Count; i++)
        {
            if (state.CompareWith(states[i]))
            {
                id = i;
            }
        }

        return id;
    }

    int ActionToId(CreatureAction action)
    {
        int id = -1;

        for(int i = 0; i < actions.Count; i++)
        {
            if (action.CompareWith(actions[i]))
            {
                id = i;
            }
        }

		if (id == -1)
			Debug.Log ("Action : "+ action.rawAction + " target : " + action.target);

        return id;
    }

    float Q(CreatureState s, CreatureAction a)
    {
		try{
			return _q[StateToId(s), ActionToId(a)];
		}
		catch{
			Debug.LogError ("Error with state : " + StateToId(s) + " and action : " + ActionToId(a));
			return -1;
		}
    }

    void SetQ(CreatureState s, CreatureAction a, float value)
    {
        _q[StateToId(s), ActionToId(a)] = value;
    }

    int R(CreatureState s, CreatureAction a)
    {
        return _r[StateToId(s), ActionToId(a)];
    }

	public IEnumerator QLearn()
    {
        _learning = true; // flag to unable the coroutine to be started more than once

		int mainNeedId = (int)(_body.state.mainNeed);

        CreatureState previousState = new CreatureState();
        previousState.SetState(_body.state);

		_body.currentNeed = _body.state.mainNeed;

        while (_body.state.needs[mainNeedId])
        {
            // sélectionner l'action depuis l'état en cours (en exploration ou en apprentissage)
            CreatureAction action = SuggestDecliningTemp(previousState);

            CreatureState nextState = new CreatureState();
            Act(action);

            while (_body._acting || _body._moving)
            {
                yield return null;
            }

			nextState.SetState (_body.state);

            float q = Q(previousState, action);
            float maxQ = MaxQ(nextState);
            int r = R(previousState, action);

            if(r > 0)
            {
                switch (action.rawAction)
                {
                    case CreatureRawAction.EATING:
                        r += previousState.target.GetComponent<InteractiveItem>().eatReward;
                        break;
                    case CreatureRawAction.SLEEPING:
                        r += previousState.target.GetComponent<InteractiveItem>().sleepReward;
                        break;
                    case CreatureRawAction.POOPING:
                        r += previousState.target.GetComponent<InteractiveItem>().poopReward;
                        break;
                    case CreatureRawAction.PLAYING:
                        r += previousState.target.GetComponent<InteractiveItem>().playReward;
                        break;
                }
            }

            float value = q + _alpha * (r + _gamma * maxQ - q);
            SetQ(previousState, action, value);

            previousState.SetState(nextState);

            _nbOfExperience++;
        }
        
        _learning = false;
    }

    float MaxQ(CreatureState state)
    {
        float maxValue = float.MinValue;
        
        for(int i = 0; i < actions.Count; i++)
        {
            float value = Q(state, actions[i]);
            
            if(value > maxValue)
            {
                maxValue = value;
            }
        }

        return maxValue;
    }

    CreatureAction Policy(CreatureState state)
    {
        double maxValue = double.MinValue;
        CreatureAction policy = actions[0];

        for(int i = 0; i < actions.Count; i++)
        {
            CreatureAction action = actions[i];
            double value = Q(state, action);

            if(value > maxValue)
            {
                maxValue = value;
                policy = action;
            }
        }

        return policy;
    }

    void Act(CreatureAction action)
    {
        switch (action.rawAction)
        {
            case CreatureRawAction.EATING:
                StartCoroutine(_body.Eat(_body.state.target));
                break;
            case CreatureRawAction.SLEEPING:
                StartCoroutine(_body.Sleep(_body.state.target));
                break;
            case CreatureRawAction.POOPING:
                StartCoroutine(_body.Poop(_body.state.target));
                break;
            case CreatureRawAction.PLAYING:
                StartCoroutine(_body.Play(_body.state.target));
                break;
            case CreatureRawAction.MOVING:
                StartCoroutine(_body.MoveTo(action.target));
                break;
            default:
                break;
        }
    }

    void PrintResult()
    {
        Debug.Log("Printing results");

        for(int i = 0; i < _q.GetLength(0); i++)
        {
            string output = "out from " + states[i].mainNeed + "|" + states[i].target + ": ";

            for(int j = 0; j < _q.GetLength(1); j++)
            {
               output += _q[i, j] + " ";
            }

            Debug.Log(output);
        }
    }

    void ShowPolicy()
    {
        Debug.Log("showPolicy");

        for(int i = 0; i < states.Count; i++)
        {
            string from = "need :" + states[i].mainNeed.ToString();
            from += " target :" + states[i].target.ToString();

            string to = Policy(states[i]).rawAction.ToString();

            Debug.Log("when " + from + " do :" + to); 
        }
    }

    float CalculateSigma(CreatureState s, float temperature)
    {
        float sigma = 0;

        for(int j = 0; j < actions.Count; j++)
        {
            sigma += Mathf.Exp(Q(s, actions[j]) / temperature);
        }

        return sigma;
    }

    CreatureAction Boltzmann(CreatureState s, float temperature)
    {
        float sigma = CalculateSigma(s, temperature);
		float p = UnityEngine.Random.Range(0,1000) / 1000.0f;
		float sum = 0;
        int j = 0;
        CreatureAction temp = new CreatureAction();

        while(sum < p)
        {
            float prob = Mathf.Exp(Q(s, actions[j]) / temperature) / sigma;
            sum += prob;
            temp = actions[j];
            j++;
        }

        return temp;
    }

    CreatureAction SuggestDecliningTemp(CreatureState s)
    {
        return Boltzmann(s, DecliningTemperature());
    }

    float DecliningTemperature()
    {
        if(_nbOfExperience >= _TEMPERATURE_CEILING)
            return _MIN_TEMPERATURE;
        else
        {
            float e = _nbOfExperience / _TEMPERATURE_CEILING;
            return (_MIN_TEMPERATURE + (1-e) * (_MAX_TEMPERATURE-_MIN_TEMPERATURE));
        }
    }
}