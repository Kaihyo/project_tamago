﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureBody : MonoBehaviour {

    [HideInInspector] public CreatureStats stats;
    public CreatureState state;
    private CreatureFace _face;

    public bool _acting = false;
    private bool _forcedActing = false;
    public bool _moving = false;
	public CreatureNeed currentNeed;

    void Start()
    {
        stats = GetComponent<CreatureStats>();
        _face = GetComponent<CreatureFace>();
    }

    void Update()
    {
        stats.UpdateStats();
        state.mainNeed = stats.CheckMainNeed();
        UpdateState();

        if (_acting)
        {
            _face.UpdateFace(state.currentAction, _forcedActing);
        }
        else
        {
            //_face.UpdateFace(stats.CheckMainNeed(), stats);
			_face.UpdateFace (currentNeed, stats);
        }
        
        /*if(stats.Stamina == 0 && !_forcedActing)
        {
            StopAllCoroutines();
            StartCoroutine(Sleep());
        }

        if(stats.Hygiene == 0 && !_forcedActing)
        {
            StopAllCoroutines();
            StartCoroutine(Poop());
        }*/
    }

    void UpdateState()
    {
        state.needs[0] = stats.Hunger < 0.5 ? true : false;
        state.needs[1] = stats.Stamina < 0.5 ? true : false;
        state.needs[2] = stats.Hygiene < 0.5 ? true : false;
        state.needs[3] = stats.Entertainment < 0.5 ? true : false;
    }

    public IEnumerator Eat(GameObject target)
    {
        _acting = true;
        state.currentAction = CreatureRawAction.EATING;

        float duration = 5; // action duration in seconds
        InteractiveItem targetProperties = target.GetComponent<InteractiveItem>();
        float newHungerValue = Mathf.Clamp01(stats.Hunger + targetProperties.statsValue.hungerRate);

        StatsRates eatingStatsRates = new StatsRates();
        eatingStatsRates.name = "Eating";
        eatingStatsRates.hungerRate = targetProperties.statsValue.hungerRate / duration;
        eatingStatsRates.staminaRate = -0.1f / duration;
        eatingStatsRates.hygieneRate = -0.15f / duration;
        eatingStatsRates.entertainmentRate = -0.07f / duration;

        stats.currentStatsRates = eatingStatsRates;

        while(stats.Hunger < newHungerValue)
        {
            yield return null;
        }

        UpdateState();

        stats.currentStatsRates = stats.statsRatesList[0];
        _acting = false;
        state.currentAction = CreatureRawAction.IDLE;
    }

    public IEnumerator Sleep() // When Stamina reaches 0
    {
        _acting = true;
        _forcedActing = true;
        state.currentAction = CreatureRawAction.SLEEPING;

        float newStaminaValue = 0.5f;

        stats.currentStatsRates = stats.statsRatesList[1];
        
        while (stats.Stamina < newStaminaValue)
        {
            yield return null;
        }

        UpdateState();

        stats.currentStatsRates = stats.statsRatesList[0];
        _acting = false;
        _forcedActing = false;
        state.currentAction = CreatureRawAction.IDLE;
    }

    public IEnumerator Sleep(GameObject target)
    {
        _acting = true;
        state.currentAction = CreatureRawAction.SLEEPING;

        float duration = 10;
        InteractiveItem targetProperties = target.GetComponent<InteractiveItem>();
        float newStaminaValue = Mathf.Clamp01(stats.Stamina + targetProperties.statsValue.staminaRate);

        StatsRates sleepingStatsRates = new StatsRates();
        sleepingStatsRates.name = "Sleeping";
        sleepingStatsRates.hungerRate = -0.15f / duration;
        sleepingStatsRates.staminaRate = targetProperties.statsValue.staminaRate / duration;
        sleepingStatsRates.hygieneRate = -0.1f / duration;
        sleepingStatsRates.entertainmentRate = 0;

        stats.currentStatsRates = sleepingStatsRates;

        while (stats.Stamina < newStaminaValue) // TODO : if starving, stop sleeping
        {
            yield return null;
        }

        UpdateState();

        stats.currentStatsRates = stats.statsRatesList[0];
        _acting = false;
        state.currentAction = CreatureRawAction.IDLE;
    }

    public IEnumerator Poop() // when hygiene reaches 0
    {
        _acting = true;
        _forcedActing = true;
        state.currentAction = CreatureRawAction.POOPING;

        float newHygieneValue = 0.5f;

        stats.currentStatsRates = stats.statsRatesList[2];

        while (stats.Hygiene < newHygieneValue)
        {
            yield return null;
        }

        UpdateState();

        stats.currentStatsRates = stats.statsRatesList[0];
        _acting = false;
        _forcedActing = false;
        state.currentAction = CreatureRawAction.IDLE;
    }

    public IEnumerator Poop(GameObject target)
    {
        _acting = true;
        state.currentAction = CreatureRawAction.POOPING;

        float duration = 5;
        InteractiveItem targetProperties = target.GetComponent<InteractiveItem>(); 
        float newHygieneValue = Mathf.Clamp01(stats.Hygiene + targetProperties.statsValue.hygieneRate);

        StatsRates poopingStatsRates = new StatsRates();
        poopingStatsRates.name = "Pooping";
        poopingStatsRates.hungerRate = -0.15f / duration;
        poopingStatsRates.staminaRate = 0;
        poopingStatsRates.hygieneRate = targetProperties.statsValue.hygieneRate / duration;
        poopingStatsRates.entertainmentRate = 0;

        stats.currentStatsRates = poopingStatsRates;

        while(stats.Hygiene < newHygieneValue)
        {
            yield return null;
        }

        UpdateState();

        stats.currentStatsRates = stats.statsRatesList[0];
        _acting = false;
        state.currentAction = CreatureRawAction.IDLE;
    }

    public IEnumerator Play(GameObject target)
    {
        _acting = true;
        state.currentAction = CreatureRawAction.PLAYING;

        float duration = 10;
        InteractiveItem targetProperties = target.GetComponent<InteractiveItem>();
        float newEntertainmentValue = Mathf.Clamp01(stats.Entertainment + targetProperties.statsValue.entertainmentRate);
        
        StatsRates playingStatsRates = new StatsRates();
        playingStatsRates.name = "Playing";
        playingStatsRates.hungerRate = -0.2f / duration;
        playingStatsRates.staminaRate = -0.1f / duration;
        playingStatsRates.hygieneRate = -0.15f / duration;
        playingStatsRates.entertainmentRate = targetProperties.statsValue.entertainmentRate / duration;

        stats.currentStatsRates = playingStatsRates;

        while (stats.Entertainment < newEntertainmentValue)
        {
            yield return null;
        }

        UpdateState();

        stats.currentStatsRates = stats.statsRatesList[0];
        _acting = false;
        state.currentAction = CreatureRawAction.IDLE;
    }

    bool IsMoving()
    {
        UnityEngine.AI.NavMeshAgent agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0)
                {
                    return false;
                }
            }
        }

        return true;
    }

    public IEnumerator MoveTo(GameObject target)
    {
        _moving = true;
        state.currentAction = CreatureRawAction.MOVING;

        UnityEngine.AI.NavMeshAgent agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        agent.speed *= GameManager.Instance.gameSpeed;
        agent.destination = target.transform.position;
        
        while (IsMoving())
        {
            yield return null;
        }

        _moving = false;
        state.currentAction = CreatureRawAction.IDLE;
        state.target = target;
    }
}