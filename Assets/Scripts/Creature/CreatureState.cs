﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CreatureState{

    public CreatureNeed mainNeed = CreatureNeed.NOTHING;
    public GameObject target;
    [HideInInspector] public CreatureRawAction currentAction = CreatureRawAction.IDLE; // Utilisé dans la gestion de l'expression de la créature

    [HideInInspector]public bool[] needs = new bool[4]; // id 1 = hungry, id 2 = tired, id 3 = dirty, id 4 = bored

    public bool CompareWith(CreatureState other)
    {
        bool output = mainNeed == other.mainNeed ? true : false;
        output &= target == other.target ? true : false;

        return output;
    }

    public void SetState(CreatureState other)
    {
        mainNeed = other.mainNeed;
        target = other.target;
        needs = other.needs;
        currentAction = other.currentAction;
    }
}
