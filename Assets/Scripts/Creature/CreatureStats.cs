﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureStats : MonoBehaviour {

    const float STARVING_TRESHOLD = 0.25f;
    const float NEED_TRESHOLD = 0.5f;

    public float Hunger { get;  set; }
    public float Stamina { get; private set; }
    public float Hygiene { get; private set; }
    public float Entertainment { get; private set; }

    public List<StatsRates> statsRatesList;
    public StatsRates currentStatsRates;

    void Start()
    {
        InitStats();

        if(statsRatesList == null || statsRatesList.Count == 0)
        {
            InitStatsRates();
        }

        if(currentStatsRates.name == "Default")
        {
            currentStatsRates = statsRatesList[0];
        }
    }

    void InitStats()
    {
        Hunger = 1;
        Stamina = 1;
        Hygiene = 1;
        Entertainment = 1;
    }

    void InitStatsRates()
    {
        statsRatesList = new List<StatsRates>();

        StatsRates newStatsRates = new StatsRates();
        newStatsRates.name = "Idle";
        newStatsRates.hungerRate = -0.0085f;
        newStatsRates.staminaRate = -0.005f;
        newStatsRates.hygieneRate = -0.007f;
        newStatsRates.entertainmentRate = -0.01f;

        statsRatesList.Add(newStatsRates);
    }

    public void UpdateStats()
    {
        Hunger = Mathf.Clamp01(Hunger + currentStatsRates.hungerRate * Time.deltaTime * GameManager.Instance.gameSpeed);
        Stamina = Mathf.Clamp01(Stamina + currentStatsRates.staminaRate * Time.deltaTime * GameManager.Instance.gameSpeed);
        Hygiene = Mathf.Clamp01(Hygiene + currentStatsRates.hygieneRate * Time.deltaTime * GameManager.Instance.gameSpeed);
        Entertainment = Mathf.Clamp01(Entertainment + currentStatsRates.entertainmentRate * Time.deltaTime * GameManager.Instance.gameSpeed);
    }

    public CreatureNeed CheckMainNeed()
    {
        if(Hunger <= STARVING_TRESHOLD)
        {
            return CreatureNeed.HUNGRY;
        }

        CreatureNeed mainNeed = CreatureNeed.NOTHING;
        float minStatValue = NEED_TRESHOLD;

        if(Hunger < minStatValue)
        {
            mainNeed = CreatureNeed.HUNGRY;
            minStatValue = Hunger;
        }

        if(Stamina < minStatValue)
        {
            mainNeed = CreatureNeed.TIRED;
            minStatValue = Stamina;
        }

        if(Hygiene < minStatValue)
        {
            mainNeed = CreatureNeed.DIRTY;
            minStatValue = Hygiene;
        }

        if(Entertainment < minStatValue)
        {
            mainNeed = CreatureNeed.BORED;
            minStatValue = Entertainment;
        }

        return mainNeed;
    }
}
