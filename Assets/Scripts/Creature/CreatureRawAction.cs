﻿public enum CreatureRawAction
{
    IDLE = 0,
    EATING = 1,
    SLEEPING = 2,
    POOPING = 3,
    PLAYING = 4,
    MOVING = 5
};