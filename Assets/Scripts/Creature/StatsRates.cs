﻿using System;

[Serializable]
public class StatsRates{
	public string name = "Idle";
	public float hungerRate = 0;
	public float staminaRate = 0;
	public float hygieneRate = 0;
	public float entertainmentRate = 0;
}