IA avec système d'apprentissage par renforcement (Q-Learning + Boltzmann)
Features :
- exploration de l'environnement et de ses besoins
- apprentissage en fonction du contexte
- exploitation des connaissances acquises de plus en plus fréquente
Axes d'amélioration :
- équilibrage des récompenses
- stabilisation du jeu (crash d'origine inconnue qui survient de manière aléatoire)
- mécaniques un peu plus punitive